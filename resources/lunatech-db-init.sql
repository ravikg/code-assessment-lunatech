DROP DATABASE IF EXISTS lunatech;
CREATE DATABASE lunatech;
USE lunatech;

CREATE TABLE IF NOT EXISTS `country` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(45) NOT NULL,
  `name` VARCHAR(200) NOT NULL,
  `continent` VARCHAR(45) NULL,
  `wikipedia_link` TEXT NULL,
  `keywords` TEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB;


LOAD DATA LOCAL INFILE 'countries.csv' INTO TABLE country FIELDS TERMINATED BY ','  ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES;

CREATE TABLE IF NOT EXISTS `airport` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ident` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `name` VARCHAR(200) NOT NULL,
  `latitude_deg` DOUBLE NULL,
  `longitude_deg` DOUBLE NULL,
  `elevation_ft` INT NULL,
  `continent` VARCHAR(45) NOT NULL,
  `iso_country` VARCHAR(45) NOT NULL,
  `iso_region` VARCHAR(45) NOT NULL,
  `municipality` VARCHAR(200) NULL,
  `scheduled_service` VARCHAR(45) NULL,
  `gps_code` VARCHAR(45) NULL,
  `iata_code` VARCHAR(45) NULL,
  `local_code` VARCHAR(45) NULL,
  `home_link` TEXT NULL,
  `wikipedia_link` TEXT NULL,
  `keywords` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_country_code_idx` (`iso_country` ASC),
  CONSTRAINT `fk_country_code`
    FOREIGN KEY (`iso_country`)
    REFERENCES `country` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


LOAD DATA LOCAL INFILE 'airports.csv' INTO TABLE airport FIELDS TERMINATED BY ','  ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES;


CREATE TABLE IF NOT EXISTS `runway` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `airport_ref` INT NOT NULL,
  `airport_ident` VARCHAR(45) NOT NULL,
  `length_ft` INT NULL,
  `width_ft` INT NULL,
  `surface` VARCHAR(200) NULL,
  `lighted` TINYINT(1) NULL,
  `closed` TINYINT(1) NULL,
  `le_ident` VARCHAR(45) NULL,
  `le_latitude_deg` DOUBLE NULL,
  `le_longitude_deg` DOUBLE NULL,
  `le_elevation_ft` INT NULL,
  `le_heading_degT` DOUBLE NULL,
  `le_displaced_threshold_ft` INT NULL,
  `he_ident` VARCHAR(45) NULL,
  `he_latitude_deg` DOUBLE NULL,
  `he_longitude_deg` DOUBLE NULL,
  `he_elevation_ft` INT NULL,
  `he_heading_degT` INT NULL,
  `he_displaced_threshold_ft` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_airport_ref_id_idx` (`airport_ref` ASC),
  CONSTRAINT `fk_airport_ref_id`
    FOREIGN KEY (`airport_ref`)
    REFERENCES `airport` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


LOAD DATA LOCAL INFILE 'runways.csv' INTO TABLE runway FIELDS TERMINATED BY ','  ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES;
