package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ravi on 10/17/16.
 */
@Entity
public class Country extends Model {

  @EmbeddedId
  private long id;

  @Id
  private String code;
  private String name;
  private String continent;
  private String wikipediaLink;
  private String keywords;

  @OneToMany(mappedBy = "country")
  @JsonIgnore
  public List<Airport> airports;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContinent() {
    return continent;
  }

  public void setContinent(String continent) {
    this.continent = continent;
  }

  public String getWikipediaLink() {
    return wikipediaLink;
  }

  public void setWikipediaLink(String wikipediaLink) {
    this.wikipediaLink = wikipediaLink;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }

  public Country() {
  }

  public static Model.Finder<Long, Country> find = new Finder<>(Country.class);

}
