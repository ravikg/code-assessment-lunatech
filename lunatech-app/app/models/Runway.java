package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by ravi on 10/17/16.
 */
@Entity
public class Runway extends Model {

  @Id
  private long id;
  private int airportRef;

  @ManyToOne
  @JoinColumn(name = "airport_ref")
  //@JsonIgnore
  public Airport airport;

  private String airportIdent;
  private int lengthFt;
  private int widthFt;
  private String surface;
  private boolean lighted;
  private boolean closed;
  private String leIdent;
  private double leLatitudeDeg;
  private double leLongitudeDeg;
  private int leElevationFt;
  @Column(name = "le_heading_degT")
  private double leHeadingDegT;
  private int leDisplacedThresholdFt;
  private String heIdent;
  private double heLatitudeDeg;
  private double heLongitudeDeg;
  private int heElevationFt;
  @Column(name = "he_heading_degT")
  private int heHeadingDegT;
  private int heDisplacedThresholdFt;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getAirportRef() {
    return airportRef;
  }

  public void setAirportRef(int airportRef) {
    this.airportRef = airportRef;
  }

  public Airport getAirport() {
    return airport;
  }

  public void setAirport(Airport airport) {
    this.airport = airport;
  }

  public String getAirportIdent() {
    return airportIdent;
  }

  public void setAirportIdent(String airportIdent) {
    this.airportIdent = airportIdent;
  }

  public int getLengthFt() {
    return lengthFt;
  }

  public void setLengthFt(int lengthFt) {
    this.lengthFt = lengthFt;
  }

  public int getWidthFt() {
    return widthFt;
  }

  public void setWidthFt(int widthFt) {
    this.widthFt = widthFt;
  }

  public String getSurface() {
    return surface;
  }

  public void setSurface(String surface) {
    this.surface = surface;
  }

  public boolean isLighted() {
    return lighted;
  }

  public void setLighted(boolean lighted) {
    this.lighted = lighted;
  }

  public boolean isClosed() {
    return closed;
  }

  public void setClosed(boolean closed) {
    this.closed = closed;
  }

  public String getLeIdent() {
    return leIdent;
  }

  public void setLeIdent(String leIdent) {
    this.leIdent = leIdent;
  }

  public double getLeLatitudeDeg() {
    return leLatitudeDeg;
  }

  public void setLeLatitudeDeg(double leLatitudeDeg) {
    this.leLatitudeDeg = leLatitudeDeg;
  }

  public double getLeLongitudeDeg() {
    return leLongitudeDeg;
  }

  public void setLeLongitudeDeg(double leLongitudeDeg) {
    this.leLongitudeDeg = leLongitudeDeg;
  }

  public int getLeElevationFt() {
    return leElevationFt;
  }

  public void setLeElevationFt(int leElevationFt) {
    this.leElevationFt = leElevationFt;
  }

  public double getLeHeadingDegT() {
    return leHeadingDegT;
  }

  public void setLeHeadingDegT(double leHeadingDegT) {
    this.leHeadingDegT = leHeadingDegT;
  }

  public int getLeDisplacedThresholdFt() {
    return leDisplacedThresholdFt;
  }

  public void setLeDisplacedThresholdFt(int leDisplacedThresholdFt) {
    this.leDisplacedThresholdFt = leDisplacedThresholdFt;
  }

  public String getHeIdent() {
    return heIdent;
  }

  public void setHeIdent(String heIdent) {
    this.heIdent = heIdent;
  }

  public double getHeLatitudeDeg() {
    return heLatitudeDeg;
  }

  public void setHeLatitudeDeg(double heLatitudeDeg) {
    this.heLatitudeDeg = heLatitudeDeg;
  }

  public double getHeLongitudeDeg() {
    return heLongitudeDeg;
  }

  public void setHeLongitudeDeg(double heLongitudeDeg) {
    this.heLongitudeDeg = heLongitudeDeg;
  }

  public int getHeElevationFt() {
    return heElevationFt;
  }

  public void setHeElevationFt(int heElevationFt) {
    this.heElevationFt = heElevationFt;
  }

  public int getHeHeadingDegT() {
    return heHeadingDegT;
  }

  public void setHeHeadingDegT(int heHeadingDegT) {
    this.heHeadingDegT = heHeadingDegT;
  }

  public int getHeDisplacedThresholdFt() {
    return heDisplacedThresholdFt;
  }

  public void setHeDisplacedThresholdFt(int heDisplacedThresholdFt) {
    this.heDisplacedThresholdFt = heDisplacedThresholdFt;
  }

  public Runway() {
  }

  public static Model.Finder<Long, Runway> find = new Finder<>(Runway.class);
}
