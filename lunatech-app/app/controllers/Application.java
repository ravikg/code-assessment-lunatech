package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.databind.JsonNode;
import models.Country;
import models.Runway;
import play.cache.CacheApi;
import play.cache.Cached;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

import static play.libs.Json.toJson;

/**
 * Created by ravi on 10/17/16.
 */
public class Application extends Controller {

  private CacheApi cache;

  @Inject
  public Application(CacheApi cache) {
    this.cache = cache;
  }

  @Cached(key = "homePage")
  public Result index() {
    return ok(views.html.index.render("Home | Lunatech App"));
  }

  public Result query() {
    return ok(views.html.query.render("Query | Lunatech App"));
  }

  public Result report() {
    return ok(views.html.report.render("Reports | Lunatech App"));
  }

  /**
   * Given a country code or name(partial/fuzzy)
   * get list of runways of all airports in that country
   * @param ccn : country code or name
   * @return json of country and list of runways
   */
  public Result getRunways(String ccn) {
    //caching at high level, it can be changed to country code level
    JsonNode responseCache = cache.getOrElse("lunatech.runways." + ccn, () -> {
      //1st search if there is any country with code=ccn
      Country country = Country.find.where().eq("code", ccn).findUnique();
      //if country is null then search for countries name
      if (country == null) {
        List<Country> countries = Country.find.where()
            .like("name", "%" + ccn + "%").findList();
        //return the 1st country from the list
        if (countries.size() > 0) country = countries.get(0);
      }

      //get list of runways
      List<Runway> runways = new ArrayList<>();
      if (country != null) {
        runways = country.airports.stream().map(a -> a.runways)
            .flatMap(List::stream).collect(Collectors.toList());
      }
      //create response
      Map<String, Object> response = new HashMap<>();
      response.put("country", country);
      response.put("runways", runways);
      return toJson(response);
    });
    return ok(responseCache);
  }

  /**
   * This generates data for various report sections
   * @return json of high and low airport counts countries
   */
  public Result getReports() {
    JsonNode responseCache = cache.getOrElse("lunatech.reports", () -> {
      //initialize response
      Map<String, Object> response = new HashMap<>();

      //sql query to get 10 countries with highest airport count
      String sqlQuery = "select * from (select iso_country, count(*) as num_airport from airport " +
          "group by iso_country order by num_airport desc limit 10) ap, country c where c.code=ap.iso_country;";
      List<SqlRow> sqlRows = Ebean.createSqlQuery(sqlQuery).findList();
      response.put("countriesHigh", sqlRows);

      //sql query to get 10 countries with lowest airport count
      sqlQuery = "select * from (select iso_country, count(*) as num_airport from airport " +
          "group by iso_country order by num_airport asc limit 10) ap, country c where c.code=ap.iso_country;";
      sqlRows = Ebean.createSqlQuery(sqlQuery).findList();
      response.put("countriesLow", sqlRows);
      return toJson(response);
    });
    return ok(responseCache);
  }

  /**
   * method to get list of all countries
   * @return json list of countries
   */
  public Result getCountries() {
    JsonNode responseCache = cache.getOrElse("lunatech.countries", () -> {
      //get all countries
      List<Country> countries = Country.find.orderBy("name asc").findList();
      return toJson(countries);
    });
    return ok(responseCache);
  }

  /**
   * method to get all runway surfaces with its count for a given country code
   * @param countryCode country code
   * @return json list of surfaces and its count
   */
  public Result getRunwaySurfaceTypes(String countryCode) {
    JsonNode responseCache = cache.getOrElse("lunatech.runwaysurfacetype." + countryCode, () -> {
      //sql query to get all runway surfaces and its count for a country
      String sqlQuery = "select c.code, c.name, r.surface, count(*) as num_surface from runway r " +
          "left join airport a on r.airport_ref = a.id left join country c on c.code=a.iso_country " +
          "where c.code = :countryCode group by r.surface, c.code order by num_surface desc;";

      List<SqlRow> sqlRows = Ebean.createSqlQuery(sqlQuery)
          .setParameter("countryCode", countryCode)
          .findList();
      return toJson(sqlRows);
    });
    return ok(responseCache);
  }

  public Result getRunwayIdent() {
    JsonNode responseCache = cache.getOrElse("lunatech.runwayident", () -> {
      //sql query to get top 10 common runway identification types
      String sqlQuery = "select le_ident, count(*) as num_le_ident from runway group by le_ident " +
          "order by num_le_ident desc limit 10;";

      List<SqlRow> sqlRows = Ebean.createSqlQuery(sqlQuery).findList();
      return toJson(sqlRows);
    });
    return ok(responseCache);
  }
}
