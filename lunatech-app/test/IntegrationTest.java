import org.junit.*;

import java.util.HashMap;
import java.util.Map;

import static play.test.Helpers.*;
import static org.junit.Assert.*;

/**
 * Integration test for the running application
 */
public class IntegrationTest {

  //app config settings map used for testing
  Map<String, Object> settings = new HashMap<>();

  /**
   * initialize the configs
   */
  @Before
  public void setupApp() {
    settings.put("db.default.url", "jdbc:mysql://localhost/lunatech");
    settings.put("db.default.username", "root");
    settings.put("db.default.password", "");
    settings.put("db.default.driver", "com.mysql.jdbc.Driver");
    settings.put("play.evolution.db.default.enabled", "false");
    settings.put("ebean.default", "models.*");
  }


  /**
   * test all pages
   */
  @Test
  public void testIndex() {
    running(testServer(3333, fakeApplication(settings)), HTMLUNIT, browser -> {
      browser.goTo("http://localhost:3333");
      assertTrue(browser.pageSource().contains("Home | Lunatech App"));
    });
  }

  @Test
  public void testQuery() {
    running(testServer(3333, fakeApplication(settings)), HTMLUNIT, browser -> {
      browser.goTo("http://localhost:3333/query");
      assertTrue(browser.pageSource().contains("Query | Lunatech App"));
    });
  }

  @Test
  public void testReport() {
    running(testServer(3333, fakeApplication(settings)), HTMLUNIT, browser -> {
      browser.goTo("http://localhost:3333/report");
      assertTrue("failed..", browser.pageSource().contains("Reports | Lunatech App"));
    });
  }

}
