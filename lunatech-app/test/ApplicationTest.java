import org.junit.*;

import com.fasterxml.jackson.databind.JsonNode;

import play.libs.Json;
import play.mvc.*;
import play.test.*;
import play.twirl.api.Content;

import static play.test.Helpers.*;
import static org.junit.Assert.*;


/**
 * JUnit tests that can call all api method of the app.
 */
public class ApplicationTest extends WithApplication {

  /**
   * Simple Test for the HTML rendering
   */
  @Test
  public void renderTemplate() {
    Content indexHtml  = views.html.index.render("Home | Lunatech App");
    Content queryHtml  = views.html.index.render("Query | Lunatech App");
    Content reportHtml = views.html.index.render("Report | Lunatech App");
    assertEquals("text/html", indexHtml.contentType());
    assertEquals("text/html", queryHtml.contentType());
    assertEquals("text/html", reportHtml.contentType());
    assertTrue(indexHtml.body().contains("Home | Lunatech App"));
    assertTrue(queryHtml.body().contains("Query | Lunatech App"));
    assertTrue(reportHtml.body().contains("Report | Lunatech App"));
  }

  /**
   * Test the query api
   * connects to local database and fetches the data
   */
  @Test
  public void testQueryApi() {
    //get runways for IO (British Indian Ocean Territory)
    Result result = new controllers.Application().getRunways("IO");
    //Runways data for IO (British Indian Ocean Territory)
    String runwaysText = "{\"country\":{\"id\":302635,\"code\":\"IO\",\"name\":\"British Indian Ocean Territory\"," +
        "\"continent\":\"AS\",\"wikipediaLink\":\"http://en.wikipedia.org/wiki/British_Indian_Ocean_Territory\"," +
        "\"keywords\":\"\"},\"runways\":[{\"id\":236822,\"airportRef\":2880,\"airport\":{\"id\":2880,\"ident\":\"FJDG\"," +
        "\"type\":\"medium_airport\",\"name\":\"Diego Garcia Naval Support Facility\",\"latitudeDeg\":-7.313270092010498," +
        "\"longitudeDeg\":72.41110229492188,\"elevationFt\":9,\"continent\":\"AS\",\"isoCountry\":\"IO\"," +
        "\"isoRegion\":\"IO-U-A\",\"municipality\":\"Diego Garcia\",\"scheduledService\":\"no\",\"gpsCode\":\"FJDG\"," +
        "\"iataCode\":\"NKW\",\"localCode\":\"\",\"homeLink\":\"\",\"wikipediaLink\":\"http://en.wikipedia.org/wiki/Diego_Garcia\"," +
        "\"keywords\":\"Chagos\"},\"airportIdent\":\"FJDG\",\"lengthFt\":12003,\"widthFt\":200,\"surface\":\"CON\"," +
        "\"lighted\":true,\"closed\":false,\"leIdent\":\"13\",\"leLatitudeDeg\":-7.30457,\"leLongitudeDeg\":72.397," +
        "\"leElevationFt\":9,\"leHeadingDegT\":121.7,\"leDisplacedThresholdFt\":0,\"heIdent\":\"31\"," +
        "\"heLatitudeDeg\":-7.32196,\"heLongitudeDeg\":72.4252,\"heElevationFt\":9,\"heHeadingDegT\":302,\"heDisplacedThresholdFt\":0}]}";
    assertEquals(OK, result.status());
    assertEquals("application/json", result.contentType().get());
    assertEquals("UTF-8", result.charset().get());
    assertTrue(contentAsString(result).equals(runwaysText));
  }

  /**
   * Test the report api
   * Check 10 countries in high and low runway count
   */
  @Test
  public void testReport1Api() {
    Result result = new controllers.Application().getReports();
    JsonNode json = Json.parse(contentAsString(result));
    JsonNode nodeHigh = json.findPath("countriesHigh");
    JsonNode nodeLow = json.findPath("countriesLow");
    assertTrue(nodeHigh.isArray());
    assertTrue(nodeLow.isArray());
    assertEquals(10, nodeHigh.size());
    assertEquals(10, nodeLow.size());
    assertEquals(OK, result.status());
    assertEquals("application/json", result.contentType().get());
    assertEquals("UTF-8", result.charset().get());
  }

  /**
   * Test the countries list api
   * Check total count of countries
   */
  @Test
  public void testCountriesApi() {
    Result result = new controllers.Application().getCountries();
    JsonNode json = Json.parse(contentAsString(result));
    assertTrue(json.isArray());
    assertEquals(247, json.size());
    assertEquals(OK, result.status());
    assertEquals("application/json", result.contentType().get());
    assertEquals("UTF-8", result.charset().get());
  }

  /**
   * Test the runway surface type api
   * For a given country get and check runway surface type list
   */
  @Test
  public void testRunwaySurfaceTypes() {
    Result result = new controllers.Application().getRunwaySurfaceTypes("IO");
    JsonNode json = Json.parse(contentAsString(result));
    String rsTypeText = "[{\"code\":\"IO\",\"name\":\"British Indian Ocean Territory\",\"surface\":\"CON\",\"num_surface\":1}]";
    assertTrue(json.isArray());
    assertEquals(1, json.size());
    assertEquals(OK, result.status());
    assertEquals("application/json", result.contentType().get());
    assertEquals("UTF-8", result.charset().get());
    assertTrue(contentAsString(result).equals(rsTypeText));
  }

  /**
   * Test the runway ident list api
   * Check 10 count of runway ident
   */
  @Test
  public void testRunwayIdentApi() {
    Result result = new controllers.Application().getRunwayIdent();
    JsonNode json = Json.parse(contentAsString(result));
    assertTrue(json.isArray());
    assertEquals(10, json.size());
    assertEquals(OK, result.status());
    assertEquals("application/json", result.contentType().get());
    assertEquals("UTF-8", result.charset().get());
  }

}
