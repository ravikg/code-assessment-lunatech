name := """lunatech-app"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "mysql" % "mysql-connector-java" % "5.1.40",
  "org.codehaus.jackson" % "jackson-mapper-asl" % "1.9.13",
  "javax.json" % "javax.json-api"%"1.0"
)
