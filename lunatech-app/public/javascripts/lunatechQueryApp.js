var LunatechApp = angular.module('LunatechApp',['datatables']);
LunatechApp.controller('LunatechAppCtrl',function($scope, lunatechFactory) {

  $scope.ccn = "NL";
  $scope.runways = [];

  /**
   * function which sends the country code or name entered by the user 
   * and in response gets the details of the all runways
   */
  $scope.getRunways = function() {
    $scope.loading = true; //enabling loading
    lunatechFactory.getRunways($scope.ccn).success(function(response) {
      console.log(response);
      $scope.runways = response["runways"];
      $scope.loading = false; //disabling loading
    });
  };

  $scope.getRunways();
});

LunatechApp.factory('lunatechFactory', ['$http', function($http) {
  var lunatechFactory = {};

  //http request which call the api for getting the runways
  lunatechFactory.getRunways = function(ccn) {
    return $http.get('/runways/' + ccn);
  };

  return lunatechFactory;
}]);