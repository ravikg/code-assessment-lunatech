var LunatechApp = angular.module('LunatechApp',['datatables']);
LunatechApp.controller('LunatechAppCtrl',function($scope, lunatechFactory, DTOptionsBuilder, DTColumnDefBuilder) {

  $scope.dtOptions = DTOptionsBuilder.newOptions().withOption("order", [[ 3, "desc" ]]);
  $scope.dtOptions2 = DTOptionsBuilder.newOptions().withOption("order", [[ 1, "desc" ]]);

  $scope.countriesHigh = [];
  $scope.countriesLow = [];
  $scope.countries = []
  $scope.countryCode;
  $scope.surfaces = [];
  $scope.runwayIdents = [];

  /**
   * function which gets all the reports
   */
  $scope.getReports = function() {
    $scope.loading = true; //enabling loading

    lunatechFactory.getReports().success(function(response) {
      console.log(response);
      $scope.countriesHigh = response["countriesHigh"];
      $scope.countriesLow  = response["countriesLow"];
      $scope.loading = false; //disabling loading
    });

    lunatechFactory.getCountries().success(function(response) {
      console.log(response);
      $scope.countries = response;
    });

    lunatechFactory.getRunwayIdent().success(function(response) {
      console.log(response);
      $scope.runwayIdents = response;
    });
  };

  $scope.getSurface = function() {
    console.log($scope.countryCode);
    lunatechFactory.getSurface($scope.countryCode.code).success(function(response) {
      console.log(response);
      $scope.surfaces = response;
    });
  }

  $scope.getReports();
});


LunatechApp.factory('lunatechFactory', ['$http', function($http) {
  var lunatechFactory = {};

  //http request which call the api for getting the reports
  lunatechFactory.getReports = function() {
    return $http.get('/reports');
  };

  //http request to get all countries
  lunatechFactory.getCountries = function() {
    return $http.get('/countries');
  };

  //http request to get surfaces for given country code
  lunatechFactory.getSurface = function(cc) {
    return $http.get('/surface/' + cc);
  };

  //http request to get top 10 commong runway identifications
  lunatechFactory.getRunwayIdent = function() {
    return $http.get('/runwayident');
  };

  return lunatechFactory;
}]);